#!/bin/bash
# File Name: ../function/show_help.sh
# Author: moshan
# mail: mo_shan@yeah.net
# Created Time: 2019-04-15 16:44:16
# Function: 
#########################################################################
work_dir=/data/git/analysis_binlog
function f_show_help()
{
	echo
	echo -e "\033[33m"
	echo "Usage: bash ${script_name} [OPTION]..."
	echo
	echo "--type=value or -t=value          The value=detail | simple"
	echo "                                  For example: --type=detail,-t=detail,-t=simple,-t=simple,"
	echo "                                  The \"detail\": The results displayed are more detailed, but also take more time."
	echo "                                  The \"simple\": The results shown are simple, but save time"
	echo "                                  The default value is \"simple\". "
	echo
	echo "--binlog-dir or -bdir             Specify a directory for the binlog dir."
	echo "                                  For example: --binlog-dir=/mysql_binlog_dir,-bdir=/mysql_binlog_dir"
	echo "                                  If the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  The default value is \"Current path\". "
	echo
	echo "--binlog-file or -bfile           Specify a file for the binlog file, multiple files separated by \",\"."
	echo "                                  For example: --binlog-file=/path/mysql_binlog_file,-bfile=/path/mysql_binlog_file"
	echo "                                               --b-file=/path/mysql_binlog_file1,/path/mysql_binlog_file1"
	echo "                                  If the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  If this parameter is used, the \"--binlog-dir or -bdir\" parameter will be invalid."
	echo
	echo "--sort or -s                      Sort the results for \"INSERT | UPDATE | DELETE | Total\""
	echo "                                  The value=insert | update | delete | total"
	echo "                                  The default value is \"insert\"."
	echo
	echo "--threads or -w                   Decompress/compress the number of concurrent. For example:--threads=8"
	echo "                                  This parameter works only when there are multiple files."
	echo "                                  If you use this parameter, specify a valid integer, and the default value is \"1\"."
	echo
	echo "--help or -h                      Display this help and exit."
	echo -e "\033[0m"
	echo
}

